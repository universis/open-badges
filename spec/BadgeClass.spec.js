import { DataConfigurationStrategy, DefaultSchemaLoaderStrategy, ODataModelBuilder, SchemaLoaderStrategy } from '@themost/data';
import {ExpressDataApplication, ExpressDataContext} from '@themost/express';
import { OpenBadgeSchemaLoader } from '../src';
import { TestUtils } from '../../../dist/server/utils';
import { OpenBadgeService } from '../src/OpenBadgeService';

describe('BadgeClass', () => {
    /**
     * @type {ExpressDataApplication}
     */
    let app;
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        // get universis api container
        const container = require('../../../dist/server/app');
        app = container.get(ExpressDataApplication.name);
        // register service
        app.useService(OpenBadgeService);
        // set configuration
        app.getConfiguration().setSourceAt('settings/open-badges', {
            origin: 'https://universis.example.com/ob/v2p1/'
        });
        /**
         * @type {DefaultSchemaLoaderStrategy}
         */
        const schemaLoader = app.getConfiguration().getStrategy(SchemaLoaderStrategy);
        schemaLoader.loaders.push(new OpenBadgeSchemaLoader(schemaLoader.getConfiguration()));
        // reset schema loader
        const builder = app.getStrategy(ODataModelBuilder);
        builder.clean(true);
        builder.initializeSync();
        
    });
    beforeEach(() => {
        context = app.createContext();
    });
    afterEach(async () => {
        await context.finalizeAsync();
    });

    it('should create BadgeClass', async() => {
        await TestUtils.executeInTransaction(context, async() => {
            const newProfile = await context.model('OpenBadgeProfile').silent().save({
                name: 'Test University',
                description: 'A profile that is going to be used for testing Open Badges.'
            });
            expect(newProfile).toBeTruthy();
            const newImage = await context.model('OpenBadgeImageObject').silent().save({
                name: 'Test Image'
            });
            expect(newImage).toBeTruthy();

            let newBadgeClass = await context.model('OpenBadgeClass').silent().save({
                name: 'Awesome Badge',
                description: 'For doing awesome things with robots that people think is pretty great.',
                image: newImage,
                issuer: newProfile
            });
            newBadgeClass = await context.model('OpenBadgeClass').where('id').equal(newBadgeClass.id).silent().getItem();
            expect(newBadgeClass).toBeTruthy();
            expect(newBadgeClass.image).toEqual(newImage.id);
            expect(newBadgeClass.issuer).toEqual(newProfile.id);
            newBadgeClass.tags = [
                'Awesome',
                'Robotics'
            ];
            await context.model('OpenBadgeClass').silent().save(newBadgeClass);
            newBadgeClass = await context.model('OpenBadgeClass')
                .where('id').equal(newBadgeClass.id).expand('tags').silent().getItem();
            expect(newBadgeClass).toBeTruthy();
            expect(newBadgeClass.tags).toBeInstanceOf(Array);
            expect(newBadgeClass.tags).toEqual([
                'Awesome',
                'Robotics'
            ]);
            newBadgeClass.criteria = {
                narrative: 'To earn the **Awesome Robotics Badge**, students must construct a basic robot.'
            }
            await context.model('OpenBadgeClass').silent().save(newBadgeClass);
            newBadgeClass = await context.model('OpenBadgeClass')
                .where('id').equal(newBadgeClass.id).expand('criteria', 'tags').silent().getItem();
            const narrative = 'To earn the Awesome Robotics Badge, students must construct a basic robot.';
            newBadgeClass.criteria.narrative = narrative;
            await context.model('OpenBadgeClass').silent().save(newBadgeClass);
            newBadgeClass = await context.model('OpenBadgeClass')
                .where('id').equal(newBadgeClass.id).expand('criteria', 'tags').silent().getItem();
            expect(newBadgeClass.criteria).toBeTruthy();
            expect(newBadgeClass.criteria.narrative).toEqual(narrative);
            // do not expand
            newBadgeClass = await context.model('OpenBadgeClass')
                .where('id').equal(newBadgeClass.id).silent().getItem();
            newBadgeClass.name = 'First Badge';
            await context.model('OpenBadgeClass').silent().save(newBadgeClass);
            newBadgeClass = await context.model('OpenBadgeClass')
                .where('id').equal(newBadgeClass.id).silent().getItem();
            expect(newBadgeClass.criteria).toBeTruthy();
        });
    });

    fit('should add alignment objects', async() => {
        await TestUtils.executeInTransaction(context, async() => {
            const newProfile = await context.model('OpenBadgeProfile').silent().save({
                name: 'Test University',
                description: 'A profile that is going to be used for testing Open Badges.'
            });
            expect(newProfile).toBeTruthy();
            const newImage = await context.model('OpenBadgeImageObject').silent().save({
                name: 'Test Image'
            });
            expect(newImage).toBeTruthy();

            const newAlignment = await context.model('OpenBadgeAlignmentObject').silent().save({
                "targetName": "CCSS.ELA-Literacy.RST.11-12.3",
                "targetUrl": "http://www.corestandards.org/ELA-Literacy/RST/11-12/3",
                "targetDescription": "Follow precisely a complex multistep procedure when",
                "targetCode": "CCSS.ELA-Literacy.RST.11-12.3"
            });
            expect(newAlignment).toBeTruthy();

            let newBadgeClass = await context.model('OpenBadgeClass').silent().save({
                name: 'Awesome Badge',
                description: 'For doing awesome things with robots that people think is pretty great.',
                image: newImage,
                issuer: newProfile,
                alignment: [
                    newAlignment
                ]
            });
            newBadgeClass = await context.model('OpenBadgeClass').where('id').equal(newBadgeClass.id).expand('alignment').silent().getItem();
            expect(newBadgeClass.alignment).toBeInstanceOf(Array);
            expect(newBadgeClass.alignment[0].targetName).toEqual('CCSS.ELA-Literacy.RST.11-12.3');
        });
    });

});