import { DataConfigurationStrategy, DefaultSchemaLoaderStrategy, ODataModelBuilder, SchemaLoaderStrategy } from '@themost/data';
import { ExpressDataApplication, ExpressDataContext } from '@themost/express';
import { OpenBadgeSchemaLoader } from '../src';
import { TestUtils } from '../../../dist/server/utils';
import { OpenBadgeService } from '../src/OpenBadgeService';

describe('AlignmentObject', () => {
    /**
     * @type {ExpressDataApplication}
     */
    let app;
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        // get universis api container
        const container = require('../../../dist/server/app');
        app = container.get(ExpressDataApplication.name);
        // register service
        app.useService(OpenBadgeService);
        // set configuration
        app.getConfiguration().setSourceAt('settings/open-badges', {
            origin: 'https://universis.example.com/ob/v2p1/'
        });
        /**
         * @type {DefaultSchemaLoaderStrategy}
         */
        const schemaLoader = app.getConfiguration().getStrategy(SchemaLoaderStrategy);
        schemaLoader.loaders.push(new OpenBadgeSchemaLoader(schemaLoader.getConfiguration()));
        // reset schema loader
        const builder = app.getStrategy(ODataModelBuilder);
        builder.clean(true);
        builder.initializeSync();

    });
    beforeEach(() => {
        context = app.createContext();
    });
    afterEach(async () => {
        await context.finalizeAsync();
    });

    it('should get AlignmentObject model definition', async () => {
        const AlignmentObjects = context.model('OpenBadgeAlignmentObject');
        expect(AlignmentObjects).toBeTruthy();
        await AlignmentObjects.getItems()
    });

    it('should create AlignmentObject', async () => {
        await TestUtils.executeInTransaction(context, async () => {
            const AlignmentObjects = context.model('OpenBadgeAlignmentObject');
            const newAlignment = await AlignmentObjects.silent().insert({
                "targetName": "CCSS.ELA-Literacy.RST.11-12.3",
                "targetUrl": "http://www.corestandards.org/ELA-Literacy/RST/11-12/3",
                "targetDescription": "Follow precisely a complex multistep procedure when",
                "targetCode": "CCSS.ELA-Literacy.RST.11-12.3"
            });
            expect(newAlignment).toBeTruthy();
        });
    });

});