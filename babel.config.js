module.exports = function (api) {
    api.cache(true);
    return {
      sourceMap: 'both',
      presets: [
        [
          '@babel/preset-env', {
            targets: {
              node: 'current'
            }
          }
        ]
      ],
      plugins: [
        [
            "@babel/plugin-proposal-decorators",
            {
                "legacy": true
            }
        ]
      ]
    };
  }
