*(\*)This project is under development*

# @universis/open-badges

Implementation of [IMS Open Badges](https://openbadges.org/) for Universis Api Server

## Install

    npm i @universis/open-badges

## Usage

Register `OpenBadgeService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/open-badges#OpenBadgeService"
        }
    ]

Add `OpenBadgeSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/open-badges#OpenBadgeSchemaLoader"
                    }
                ]
            }
        }
    }