import express from 'express';
import * as axios from 'axios';

/*
OpenBadges endpoints:
- openbadges/badges/:id
- openbadges/profiles/:id
- openbadges/assertions/:id
- openbadges/badges/:id/image
- openbadges/assertions/:id/image

Registered in app.js with:
app.use('/openbadges/', openbadgesRoutes(dataApplication.getConfiguration()));

*/

/**
 * @param {ConfigurationBase} configuration
 * @returns {Router}
 */
export function openbadgesRoutes(configuration) {
    let router = express.Router();

    const formatObject = {
        "badges": (item) => ({
            "id": item.id,
            "type": "BadgeClass",
            "name": item.name,
            "description": item.description,
            "image": item.id + '/image',
            "criteria": {
                "narrative": item.criteria.narrative,
            },
            "issuer": formatObject["profiles"](item.issuer),
            "tags": item.tags
        }),
        "profiles": (item) => ({
            "id": item.id,
            "type": "Profile",
            "name": item.name,
            "url": item.url,
            "telephone": item.telephone,
            "description": item.description,
            "email": item.email,
        }),
        "assertions": (item) => ({
            "id": item.id,
            "type": "Assertion",
            "image": item.id + '/image',
            "recipient": {
                "type": "email",
                "identity": item.recipient.identity
            },
            "issuedOn": item.issuedOn,
            "verification": {
                "type": "hosted"
            },
            "badge": formatObject["badges"](item.badge)
        }),
    }

    // (context, id) => object
    const fetchObject = {
        "badges": async (context, id) => {
            const item = await context.model('OpenBadgeClasses').silent()
                .where('_id').equal(id)
                .expand('criteria', 'tags', 'image', 'issuer')
                .getItem();
            return item;
        },
        "profiles": async (context, id) => {
            const item = await context.model('OpenBadgeProfiles').silent()
                .where('_id').equal(id)
                .getItem();
            return item;
        },
        "assertions": async (context, id) => {
            const item = await context.model('OpenBadgeAssertions').silent()
                .where('_id').equal(id)
                .expand('recipient', {
                    name: "badge",
                    options: { '$expand': 'criteria,tags,image,issuer' }
                })
                .getItem();
            return item;
        }
    }

    const getObject = async (context, path, id) => {
        const object = await fetchObject[path](context, id);
        if (!object) return null;
        const formatted = formatObject[path](object)
        // add the context field to the root object
        const formattedRoot = Object.assign({
            "@context": "https://w3id.org/openbadges/v2",
        }, formatted)
        return formattedRoot;
    }

    const handlerObject = async (path, req, res, next) => {
        try {
            const object = await getObject(req.context, path, req.params.id);
            if (object) {
                return res
                    .set("Content-type", "application/json; charset=utf-8")
                    .send(JSON.stringify(object, null, 2))
            } else {
                return next(new HttpNotFoundError());
            }
        } catch (err) {
            return next(err);
        }
    }

    router.get('/badges/:id', async function (req, res, next) {
        return handlerObject("badges", req, res, next);
    });
    router.get('/profiles/:id', async function (req, res, next) {
        return handlerObject("profiles", req, res, next);
    });
    router.get('/assertions/:id', async function (req, res, next) {
        return handlerObject("assertions", req, res, next);
    });


    // (image, badge) => image
    const renderImage = {
        "svg": (image, badge) => {
            var svg = image;
            const mappings = [
                { name: "{{Badge.Name}}", value: badge.name },
                { name: "{{Badge.Issuer.Name}}", value: badge.issuer.name },
                { name: "{{Badge.SubBadges}}", value: 0 }, // TODO add this value
            ]
            svg = mappings.reduce((base, mapping) => base.replace(mapping.name, mapping.value), svg)
            return svg;
        },
        "png": (image, badge) => image
    }

    // (image, assertion) => image
    const bakeImage = {
        "svg": (image, object) => {
            const namespace = `xmlns:openbadges="http://openbadges.org"`;
            const assertionTag = `
<openbadges:assertion verify="{{Assertion.Id}}">
  <![CDATA[
{{Assertion.Data}}
  ]]>
</openbadges:assertion>
                `.trim();
            var svg = image;
            svg = renderImage["svg"](svg, object.badge)
            // insert the namespace in the first svg tag
            if (svg.indexOf(namespace) < 0) {
                svg = svg.replace(/<svg/, '<svg ' + namespace)
            }
            // insert the assertion tag before all closing svg tags
            if (svg.indexOf('<openbadges:assertion') < 0) {
                svg = svg.replace(/<\/svg>/g, assertionTag + '\n</svg>')
            }
            const objectJSON = JSON.stringify(object, null, 2);
            const mappings = [
                { name: "{{Assertion.Id}}", value: object.id },
                { name: "{{Assertion.Data}}", value: objectJSON }
            ]
            svg = mappings.reduce((base, mapping) => base.replace(mapping.name, mapping.value), svg)
            return svg;
        },
        "png": (image, assertion) => {
            // TODO implement PNG baking
            return image;
            // throw new HttpNotFoundError();
        }
    }

    const handlerImage = async (path, imageUrl, transformer, req, res, next) => {
        try {
            const object = await getObject(req.context, path, req.params.id);
            if (object) {
                // download the imageUrl
                const imageFileResponse = await axios.get(imageUrl, { responseType: 'arraybuffer' });
                if (imageFileResponse.status != 200) {
                    return next(new HttpNotFoundError()); 
                }
                const image = imageFileResponse.data;
                if (!image) {
                    return next(new HttpNotFoundError());
                } else if (imageUrl.endsWith('.png')) {
                    const transformedImage = transformer["png"](image, object);
                    return res
                        .set("Content-type", "image/png")
                        .send(transformedImage);
                } else {
                    // assume svg
                    const imageSvg = new TextDecoder("utf-8").decode(image)
                    const transformedImage = transformer["svg"](imageSvg, object);
                    return res
                        .set("Content-type", "image/svg+xml; charset=utf-8")
                        .send(transformedImage);
                }
            } else {
                return next(new HttpNotFoundError());
            }
        } catch (err) {
            return next(err);
        }
    }

    router.get('/badges/:id/image', async function (req, res, next) {
        const object = await fetchObject["badges"](req.context, req.params.id)
        const imageUrl = object.image.contentUrl
        return handlerImage("badges", imageUrl, renderImage, req, res, next);
    });
    router.get('/assertions/:id/image', async function (req, res, next) {
        const object = await fetchObject["assertions"](req.context, req.params.id)
        const imageUrl = object.badge.image.contentUrl
        return handlerImage("assertions", imageUrl, bakeImage, req, res, next);
    });

    return router
}
