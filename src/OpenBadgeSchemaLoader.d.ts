import {FileSchemaLoaderStrategy} from '@themost/data';
import { ConfigurationBase } from '@themost/common';
export declare class OpenBadgeSchemaLoader extends FileSchemaLoaderStrategy {
    constructor(config: ConfigurationBase);
}