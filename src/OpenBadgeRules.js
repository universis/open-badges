import _ from 'lodash';
import util from "util";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy } from '@themost/data';
import { TraceUtils, LangUtils } from '@themost/common';
import { QueryExpression } from '@themost/query';

/**
 * @param {ExpressDataApplication} app 
 */
export function defineRules(app) {

    const schemaLoader = app.getConfiguration()
        .getStrategy(SchemaLoaderStrategy);

    const modelDefinition = schemaLoader.getModelDefinition("Rule");
    const RuleClass = app.getConfiguration().getStrategy(ModelClassLoaderStrategy).resolve(modelDefinition);
    if (typeof RuleClass !== 'function') {
        throw new Error('The base class Rule cannot be found');
    }

    const handle = (name, DataObjectClass) => {
        const model = schemaLoader.getModelDefinition(name);
        model.DataObjectClass = DataObjectClass;
        schemaLoader.setModelDefinition(model);
    }

    class OpenBadgeRule extends RuleClass {
        constructor() {
            super();
        }

        /**
         * Validates an expression against the current student
         * @param {*} obj
         * @param {Function} done
         */
        validate(obj, done) {
            const self = this, context = self.context;
            try {
                if (_.isNil(this.checkValues)) {
                    return done(null, new ValidationResult(true));
                }
                if (_.isNil(obj)) {
                    return done(null, new ValidationResult(true));
                }
                if (_.isNil(obj.student)) {
                    return done(null, self.failure('ESTUD', 'Student data is missing.'));
                }
                /**
                 * @type {DataModel}
                 */
                const students = context.model('Student');
                /**
                 * Represents the student associated with the given data
                 * @type {Student|*}
                 */
                const student = students.convert(obj.student);
                /**
                 * Represents an array of prerequisites for the given data
                 * @type {Array}
                 */
                const values = this.checkValues.split(',');
                /**
                 * Initialize the data queryable associated with the given rule
                 * @type {DataQueryable|QueryExpression}
                 */
                const q = context.model('OpenBadgeAssertions')
                    .where('student').equal(student.getId())
                    .and('badge').in(values)
                    .prepare();

                self.excludeStudent(student, function (err, exclude) {
                    if (err) {
                        TraceUtils.error(err);
                        return done(null, self.failure('EFAIL', 'An error occurred while trying to validate rules.', 'An occurred while searching excluded students.'));
                    }
                    if (exclude) {
                        return done(null, self.success('SUCC', 'The input data meets the specified rules.', 'Student was excluded from rule validation due to specific attributes.'));
                    }
                    else {
                        q.silent().count(function (err, count) {
                            if (err) {
                                return done(null, self.failure('EFAIL', 'An error occurred while trying to validate rules.', 'An error occured while searching courses.'));
                            }
                            // keep result in data and pass this to validationResult
                            const data = {
                                "result": count,
                                "value1": self.value4
                            };
                            //get minimum of prerequisites
                            const minCount = LangUtils.parseInt(self.value4);
                            self.badges(function (err, result) {
                                if (err) {
                                    TraceUtils.error(err);
                                    result = [];
                                }
                                let message = self.formatMessage(result.map(function (x) { return x.description; }).join(', '));
                                if (minCount <= count) {
                                    //success
                                    return done(null, self.success('SUCC', message, null, data));
                                }
                                else {
                                    return done(null, self.failure('FAIL', message, null, data));
                                }
                            });
                        });
                    }
                });
            }
            catch (e) {
                done(e);
            }
        }

        badges(callback) {
            try {
                const values = this.checkValues.split(',');
                this.context.model('OpenBadgeClass').where('id').in(values).select('_id', 'name').silent().all((err, result) => {
                    if (err) { return callback(err); }
                    result.forEach(function (x) {
                        x.description = `(${x._id}) ${x.name}`;
                    });
                    callback(null, result);
                });
            }
            catch (e) {
                callback(e);
            }
        }

        /**
         * @returns {*} ...arg
         */
        formatMessage() {
            //get number of passed badges
            let s = "";
            const minCount = LangUtils.parseInt(this.value4);
            if (minCount === 1) {
                s = 'The prerequisite badge must be passed.'
            }
            else {
                s = '%s badges of the following prerequisite badges %s must be passed.'
            }
            const args = [].slice.call(arguments);
            args.unshift(minCount);
            args.unshift(this.context.__(s));
            return util.format.apply(this, args);
        }

        formatDescription(callback) {
            const self = this;
            self.badges(function (err, result) {
                if (err) {
                    TraceUtils.error(err);
                    result = [];
                }
                let message = self.formatMessage(result.map(function (x) {
                    return x.description;
                }).join(', '));
                return callback(null, message);
            });

        }
    }

    class OpenBadgeTagsRule extends RuleClass {
        constructor(obj) {
            super('Rule', obj);
        }

        // TODO replace passed/completed with acquired

        /**
         * Validates an expression against the current student
         * @param {*} obj
         * @param {function(Error=,*=)} done
         */
        validate(obj, done) {
            try {
                //done(null, new ValidationResult(false,'FAIL','Class Registration was cancelled by the user'));
                const self = this;

                const context = self.context;
                const student = context.model('Student').convert(obj.student);
                const assertions = context.model('OpenBadgeAssertion').expand("badge");
                if (_.isNil(student)) {
                    return done(null, self.failure('ESTUD', 'Student data is missing.'));
                }
                if (_.isNil(student.getId())) {
                    return done(null, self.failure('ESTUD', 'Student data cannot be found.'));
                }
                const op = this.operatorOf();
                if (_.isNil(op)) {
                    return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.', 'The specified operator is not yet implemented.'));
                }
                //get queryable object completed badges
                const q = assertions.where('student').equal(student.getId()).prepare();
                if (this.checkValues !== "-1") {
                    //filter specific tags, -1 all tags
                    // TODO debug
                    q.where('badge/tags').in(this.checkValues.split(',')).prepare();
                }
                // check operator
                let fnOperator = q[op];
                if (typeof fnOperator !== 'function') {
                    return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
                }

                const sumOf = this.value6;
                let fieldOf;

                switch (sumOf) {
                    // TODO replace comment with error
                    // case "0": 
                    //     fieldOf="sum(units) as res"; //units
                    //     break;
                    // case "2":
                    //     fieldOf="sum(hours) as res"; //hours
                    //     break;
                    // case "3":
                    //     fieldOf="sum(ects) as res"; //ects
                    //     break;
                    default:
                        fieldOf = "count(id) as res";
                }
                q.silent().select(fieldOf).first(function (err, result) {
                    if (err) {
                        TraceUtils.error(err);
                        return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.'));
                    }
                    // keep result in data and pass this to validationResult
                    const data = {
                        "result": result && result.res,
                        "operator": op,
                        "value1": self.value1,
                        "value2": self.value2
                    };

                    const q2 = (new QueryExpression().from('a')).select([
                        { res: { $value: LangUtils.parseFloat(result.res) } }
                    ]).where('res');
                    q2.$fixed = true;
                    fnOperator = q2[op];
                    //q2.where('res');
                    fnOperator.apply(q2, [LangUtils.parseFloat(self.value1), LangUtils.parseFloat(self.value2)]);
                    assertions.context.db.execute(q2, null, function (err, result) {
                        if (err) {
                            return done(err);
                        }
                        else {
                            if (result.length === 1) {
                                self.formatDescription(function (err, message) {
                                    if (err) { return done(err); }
                                    return done(null, self.success('SUCC', message, null, data));
                                });
                            }
                            else {
                                self.formatDescription(function (err, message) {
                                    if (err) { return done(err); }
                                    return done(null, self.failure('FAIL', message, null, data));
                                });
                            }
                        }
                    });

                });
            }
            catch (e) {
                done(e);
            }
        }

        formatDescription(callback) {
            const op = LangUtils.parseInt(this.ruleOperator);
            let s;
            let description;
            const self = this;
            const sumOf = this.value6;
            let fieldDescription;
            const args = [];

            switch (sumOf) {
                // case "0":
                //     fieldDescription="Units of completed badges";
                //     break;
                // case "2":
                //     fieldDescription="Hours of completed badges";
                //     break;
                // case "3":
                //     fieldDescription="ECTS of completed badges";
                //     break;
                default:
                    fieldDescription = "Number of completed badges";
            }
            switch (op) {
                case 0: s = "%s must be equal to %s"; break;
                case 1: s = "%s must contain '%s'"; break;
                case 2: s = "%s must be different from %s"; break;
                case 3: s = "%s must be greater than %s"; break;
                case 4: s = "%s must be lower than %s"; break;
                case 5: s = "%s must be between %s and %s"; break;
                case 6: s = "%s must start with '%s'"; break;
                case 7: s = "%s must not contain '%s'"; break;
                case 8: s = "%s must be greater or equal to %s"; break;
                case 9: s = "%s must be lower or equal to %s"; break;
            }
            s = this.context.__(s);
            args.push(this.context.__(fieldDescription));
            args.push(this.value1);
            args.push(this.value2);
            for (let i = 0; i < args.length; i++) {
                if (_.isNil(args[i])) {
                    args[i] = '';
                }
            }
            args.unshift(s);
            s = util.format.apply(this, args);

            this.badgeTags(function (err, result) {
                if (err) {
                    callback(err)
                }
                description = util.format("%s:%s, %s", self.context.__("Badge tags"), result, s);
                callback(null, description);
            });
        }

        badgeTags(callback) {
            const values = this.checkValues.split(',');
            if (values[0] === "-1") {
                callback(null, this.context.__("All types"));
            } else {
                callback(null, values.join(', '))
            }
        }
    }

    handle("OpenBadgeRule", OpenBadgeRule);
    handle("OpenBadgeTagsRule", OpenBadgeTagsRule);

}

class ValidationResult {
    constructor(success, code, message, innerMessage, data) {
        Object.defineProperty(this, 'type', {
            value: this.constructor.name,
            writable: true,
            enumerable: true
        });
        this.success = typeof success !== 'undefined' && success !== null ? success : false;
        this.statusCode = this.success ? 200 : 422;
        this.code = code;
        this.message = message;
        this.innerMessage = innerMessage;
        this.data = data;
    }
}