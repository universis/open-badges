import { DataObjectState, DataEventArgs } from '@themost/data';


async function beforeSaveAsync(event) {
  const context = event.model.context;
  const target = event.target;

  // check for existing educationalCredentials
  const target_expanded = await context.model('OpenBadgeClasses')
    .where('_id').equal(target._id)
    .select('educationalCredentials').expand("educationalCredentials")
    .getItem()
  let educationalCredentials = target_expanded && target_expanded["educationalCredentials"];

  // create/update the educational credentials information
  if (!educationalCredentials) educationalCredentials = {}
  educationalCredentials.competencyRequired = `${target.name}: ${target.description}`
  educationalCredentials.alternateName = `badge${target._id}`
  // educationalCredentials.EducationalCredentialType = TODO find badge value

  educationalCredentials = await context.model("EducationalOccupationalCredentials").silent()
    .save(educationalCredentials);

  target.educationalCredentials = educationalCredentials
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}