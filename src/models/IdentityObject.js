import { DataObject } from '@themost/data';

class IdentityObject extends DataObject {

    /**
     * @type {string}
     */
    id;
    /**
     * @type {string}
     */
    identity;
    /**
     * @type {string}
     */
     type = 'email';
    /**
     * @type {boolean}
     */
     hashed = false;
    /**
     * @type {string}
     */
     salt;

    constructor() {
        super();
    }

    static createHash(value, salt) {

    }

}

export default IdentityObject;