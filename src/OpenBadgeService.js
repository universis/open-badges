import { ApplicationService } from '@themost/common';
import { DataContext, FunctionContext, ODataModelBuilder } from '@themost/data';
import { ExpressDataApplication } from '@themost/express'
import { defineRules } from './OpenBadgeRules';


/**
 * @this FunctionContext
 * @param {*=} identifier
 * @returns
 */
async function newIRI(identifier) {
    /**
     * @type {DataContext}
     */
    const context = this.model.context;
    const origin = context.getConfiguration().getSourceAt('settings/open-badges/origin');
    if (origin == null) {
        throw new Error('Invalid application configuration. IRI origin is missing');
    }
    const modelToPath = {
        "OpenBadgeClass": "badges",
        "OpenBadgeProfile": "profiles",
        "OpenBadgeAssertion": "assertions",
    }
    let path = modelToPath[this.model.name]
    if (!path) {
        let entitySet = this.model.entitySet;
        if (entitySet == null) {
            const builder = context.application.getStrategy(ODataModelBuilder);
            const entitySetObject = builder.getEntityTypeEntitySet(this.model.name);
            if (entitySetObject == null) {
                throw new Error('Invalid application configuration. IRI entity type is missing');
            }
            entitySet = entitySetObject.name;
        }
        path = entitySet
    }
    let value = identifier;
    if (value == null) {
        value = await this.newid();
    }
    return new URL(`${path}/${value}`, origin).toString();
}

class OpenBadgeService extends ApplicationService {

    /**
     * @param {ExpressDataApplication} app 
     */
    constructor(app) {
        super(app);
        // extend FunctionContext
        if (typeof FunctionContext.prototype.newIRI === 'undefined') {
            FunctionContext.prototype.newIRI = newIRI;
        }

        let rulesService = app.getService(function CalculateRuleAttributesService() { });
        if (!rulesService) {
            throw new Error('CalculateRuleAttributesService service cannot be found or is inaccessible.');
        }
        const additional = {
            references: [
                {
                    "refersToIdentifier": 140,
                    "refersTo": "OpenBadge"
                },
                {
                    "refersToIdentifier": 150,
                    "refersTo": "OpenBadgeTags"
                }
            ],
            types: [
                {
                    "targetTypeIdentifier": 114,
                    "additionalTypeIdentifier": 1,
                    "targetType": "EducationalOccupationalCredentials",
                    "additionalType": "EducationalOccupationalCredentialsRule"
                }
            ],
            optionalCheckValues: [
                "OpenBadgeTags"
            ]
        }
        for (const name in additional) {
            rulesService[name].push(...additional[name])
        }

        defineRules(app)
    }
}

export {
    OpenBadgeService
}